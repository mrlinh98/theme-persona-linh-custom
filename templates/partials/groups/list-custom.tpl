<ul class="categories" style="box-shadow: none; border: 1px solid rgba(195, 200, 218, 0.38); padding-bottom: 25px;">
	<h1 class="categories-title" style="margin-top: 20px; padding-left: 4%;">Nhóm</h1>
	<div class="small-line"></div>

  {{{ each groupData.groups }}}
    <li component="groups/summary" data-slug="{groups.slug} class="row clearfix" >
      <div class="wrapper" class="d-flex flex-row">
        <a href="{config.relative_path}/groups/{groups.slug}" class="fancy-button bg-gradient2"><span>{groups.displayName}(<small>{groups.memberCount}</small>)</span></a>
    
        <ul class="members" style="list-style: none; display: inline; padding-left: 0px; position: relative;top: 1rem;">
          {{{each groups.members}}}
          <li style="display: inline;">
            <a href="{config.relative_path}/user/{groups.members.userslug}">{buildAvatar(groups.members, "sm", true)}</a>
          </li>
          {{{end}}}
          <!-- IF groups.truncated -->
          <li class="truncated" style="display: inline; position: relative; top: 10px;"><i class="fa fa-ellipsis-h"></i></li>
          <!-- ENDIF groups.truncated -->
        </ul>
    
      </div>
    </li>
  {{{ end }}}
</ul>
<a class="see-more perfect-center" href="/groups">Xem Tất Cả</a>



