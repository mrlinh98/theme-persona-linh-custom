<li component="categories/category" data-cid="{../cid}" data-numRecentReplies="1" class="row clearfix">
	<meta itemprop="name" content="{../name}">

	<div style="width: 100%;" class="dropdown show content col-xs-12 <!-- IF config.hideCategoryLastPost -->col-md-10 col-sm-12<!-- ELSE -->col-md-7 col-sm-9<!-- ENDIF config.hideCategoryLastPost -->" onMouseOver="this.className='dropdown show content col-xs-12 <!-- IF config.hideCategoryLastPost -->col-md-10 col-sm-12<!-- ELSE -->col-md-7 col-sm-9<!-- ENDIF config.hideCategoryLastPost --> open'" onMouseOut="this.className='dropdown show content col-xs-12 <!-- IF config.hideCategoryLastPost -->col-md-10 col-sm-12<!-- ELSE -->col-md-7 col-sm-9<!-- ENDIF config.hideCategoryLastPost -->'">
		<div class="icon pull-left dropdown-toggle" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="{function.generateCategoryBackground};text-align:center;cursor: pointer;">
			<i class="fa fa-fw {../icon}"></i>
		</div>

		<h2 class="title" style="margin-top: 15px;">
			<!-- IMPORT partials/categories/link.tpl -->
		</h2>

		<div class="dropdown-menu" style="<!-- IF !categories.children --> display: none; <!-- ENDIF categories.children --> border-radius: 5px; border: 1px solid rgba(156, 136, 136, 0.16); margin-top: 0px; width: 100%; box-shadow: 0 2px 6px rgba(0, 0, 0, 0.175);" aria-labelledby="dropdownMenuLink" >
			<!-- IF !config.hideSubCategories -->
				{function.generateChildrenCategories}
			<!-- ENDIF !config.hideSubCategories -->
		</div>
	
		<span class="visible-xs pull-right">
			<!-- IF ../teaser.timestampISO -->
			<a class="permalink" href="{../teaser.url}">
				<small class="timeago" title="{../teaser.timestampISO}"></small>
			</a>
			<!-- ENDIF ../teaser.timestampISO -->
		</span>
	</div>
</li>
