<!-- IF ../isSection -->
{../name}
<!-- ELSE -->
<!-- IF ../link -->
<a href="{../link}" 
  itemprop="url" 
  style="font-size: 15px;
	background-image: none;
  border-style: none;
  box-sizing: border-box;
  color: #192433;
  font-weight: 700;
  margin: 0;
  padding: 0;" 
  onMouseOver="this.style.color='#00bad6'" 
  onMouseOut="this.style.color='#192433'">
<!-- ELSE -->
<a href="{config.relative_path}/category/{../slug}" itemprop="url" class="categories-link" onMouseOver="this.style.color='#00bad6'" onMouseOut="this.style.color='#192433'">
<!-- ENDIF ../link -->
{../name}
</a>
<!-- ENDIF ../isSection -->