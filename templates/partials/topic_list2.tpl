<ul component="category" class="topic-list" itemscope itemtype="http://www.schema.org/ItemList" data-nextstart="{nextStart}" data-set="{set}">
	<h1 class="categories-title" style="margin-top: 20px; padding-left: 4%;
    left: -3rem;">Nổi bật</h1>
	<div class="small-line"></div>
	<meta itemprop="itemListOrder" content="descending">
		{{{each popularData.topics}}}
			<li component="category/topic" class="row clearfix category-item {function.generateTopicClass}" <!-- IMPORT partials/data/category.tpl -->>
				<meta itemprop="name" content="{function.stripTags, title}">

				<div class="col-md-12 col-sm-9 col-xs-10 content" style="margin: 10px 0 20px 0">
					<div class="avatar pull-left">
						<!-- IF showSelect -->
						<div class="select" component="topic/select">
							<!-- IF topics.thumb -->
							<img src="{topics.thumb}" class="thumbnail-topic user-img not-responsive" />
							<!-- ELSE -->
							<a href="{config.relative_path}/topic/{topics.slug}/{topics.bookmark}"><span class="thumbnail-topic user-img not-responsive fa-stack fa-lg" style="{function.generateCategoryBackground, topics.category}; position: relative"><i style="color:{topics.category.color}; position: absolute; top: 45%; font-size:60px" class="fa {topics.category.icon} fa-stack-1x"></i></span></a>
							<!-- ENDIF topics.thumb -->
							<i class="fa fa-check"></i>
						</div>
						<!-- ENDIF showSelect -->

						<!-- IF !showSelect -->
						<a href="{config.relative_path}/topic/{topics.slug}<!-- IF topics.bookmark -->/{topics.bookmark}<!-- ENDIF topics.bookmark -->" class="pull-left">
							<!-- IF topics.thumb -->
							<img src="{topics.thumb}" class="user-img not-responsive" />
							<!-- ELSE -->
							<a href="{config.relative_path}/topic/{topics.slug}<!-- IF topics.bookmark -->/{topics.bookmark}<!-- ENDIF topics.bookmark -->"><span class="thumbnail-topic user-img not-responsive fa-stack fa-lg" style="{function.generateCategoryBackground, topics.category}; position: relative"><i style="color:{topics.category.color}; position: absolute; top: 45%; font-size:60px" class="fa {topics.category.icon} fa-stack-1x"></i></span></a>
							<!-- ENDIF topics.thumb -->
						</a>
						<!-- ENDIF !showSelect -->
					</div>

					<h2 component="topic/header" class="title">
						<i component="topic/pinned" class="fa fa-thumb-tack <!-- IF !topics.pinned -->hide<!-- ENDIF !topics.pinned -->" title="[[topic:pinned]]"></i>
						<i component="topic/locked" class="fa fa-lock <!-- IF !topics.locked -->hide<!-- ENDIF !topics.locked -->" title="[[topic:locked]]"></i>
						<i component="topic/moved" class="fa fa-arrow-circle-right <!-- IF !topics.oldCid -->hide<!-- ENDIF !topics.oldCid -->" title="[[topic:moved]]"></i>
						{{{each icons}}}@value{{{end}}}

						<!-- IF !topics.noAnchor -->
						<a href="{config.relative_path}/topic/{topics.slug}<!-- IF topics.bookmark -->/{topics.bookmark}<!-- ENDIF topics.bookmark -->" itemprop="url" style="color: #192433" class="titleMobile"  onMouseOver="this.style.color='#00bad6'" onMouseOut="this.style.color='#192433'" >{topics.title}</a><br />
						<!-- ELSE -->
						<span>{topics.title}</span><br />
						<!-- ENDIF !topics.noAnchor -->
						
						<!-- IF topics.sliceContent -->
						<p style="font-size: 14px; color: #262e40; margin: 5px 0 0 0; margin: 1rem 0 1rem 0">{topics.sliceContent}</p>
						<!-- ENDIF topics.sliceContent -->
				
						<small style="margin-right: 0px;">{buildAvatar(topics.user, "22", true, "not-responsive")}</small>

						<small>
							<a style="font-size: 14px; color: hsl(188, 100%, 42%);" href="<!-- IF topics.user.userslug -->{config.relative_path}/user/{topics.user.userslug}<!-- ELSE -->#<!-- ENDIF topics.user.userslug -->">{topics.user.username}</a>
						</small>

						<!-- IF topics.tags.length -->
						<span class="tag-list hidden-xs">
							{{{each topics.tags}}}
							<a href="{config.relative_path}/tags/{topics.tags.valueEscaped}"><span class="tag" style="<!-- IF topics.tags.color -->color: {topics.tags.color};<!-- ENDIF topics.tags.color --><!-- IF topics.tags.bgColor -->background-color: {topics.tags.bgColor};<!-- ENDIF topics.tags.bgColor -->">{topics.tags.valueEscaped}</span></a>
							{{{end}}}
						</span>
						<!-- ENDIF topics.tags.length -->

						<small style="font-size: 12px; color: #6c757d99;" class="hidden-xs">&bull; <span class="timeago" title="{topics.timestampISO}"></small>
						<small style="font-size: 12px; color: #6c757d99;" class="visible-xs-inline"> 
							<i class="fa fa-reply"></i> &nbsp;
							<!-- IF topics.teaser.timestamp -->
							<span class="timeago" style="font-size: 12px; color: #6c757d99;" title="{topics.teaser.timestampISO}"></span> 
							<!-- ELSE -->
							<span class="timeago" style="font-size: 12px; color: #6c757d99;" title="{topics.timestampISO}"></span>
							<!-- ENDIF topics.teaser.timestamp -->
						</small>

						<!-- IF topics.viewcount -->
						<small style="font-size: 12px; color: #6c757d99;">&bull; {topics.viewcount} views</small>
						<!-- ENDIF topics.viewcount -->

					</h2>
				</div>

				<div class="mobile-stat col-xs-2 visible-xs text-right">
					<span class="human-readable-number">{topics.postcount}</span> <a href="{config.relative_path}/topic/{topics.slug}/{topics.teaser.index}"><i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</li>
			{{{end}}}


	
