<!-- IMPORT partials/breadcrumbs.tpl -->
<div data-widget-area="header">
	{{{each widgets.header}}}
	{{widgets.header.html}}
	{{{end}}}
</div>
<div class="row">

	<div class="recent col-sm-12 col-lg-12" <!-- IF isCategories -->style="display:none;"<!-- ENDIF isCategories -->>
		<h1 class="categories-title">[[pages:recents]]</h1>
		<div class="category">
			<!-- IF !recentData.topics.length -->
				<div class="alert alert-warning" id="category-no-topics">[[recent:no_recent_topics]]</div>
			<!-- ELSE -->
				<!-- IMPORT partials/topics_list.tpl -->
			<!-- ENDIF !recentData.topics.length -->

			<!-- IF config.usePagination -->
				<!-- IMPORT partials/paginator.tpl -->
			<!-- ENDIF config.usePagination -->
		</div>
	</div>

	<br />
	<br />

	<!-- IF isCategories -->
		<div class="<!-- IF widgets.sidebar.length -->col-lg-12<!-- ELSE -->col-lg-12<!-- ENDIF widgets.sidebar.length -->">
			<h1 class="categories-title">[[pages:categories]]</h1>
			<ul class="categories" itemscope itemtype="http://www.schema.org/ItemList">
				{{{each categories}}}
				<!-- IMPORT partials/categories/item.tpl -->
				{{{end}}}
			</ul>
		</div>
	<!-- ELSE -->
		<div class="<!-- IF widgets.sidebar.length -->col-lg-12<!-- ELSE -->col-lg-4<!-- ENDIF widgets.sidebar.length -->">
			<h1 class="categories-title">[[pages:categories]]</h1>
			<ul class="categories" itemscope itemtype="http://www.schema.org/ItemList">
				{{{each categories}}}
				<!-- IMPORT partials/categories/item.tpl -->
				{{{end}}}
			</ul>
		</div>
	<!-- ENDIF isCategories -->

	<div class="popular col-sm-12 col-lg-8"  <!-- IF isCategories -->style="display:none;"<!-- ENDIF isCategories -->>
		<h1 class="categories-title" <!-- IF isCategories -->style="display:none;"<!-- ENDIF isCategories -->>[[pages:populars]]</h1>
		<div class="category">
			<!-- IF !popularData.topics.length -->
				<div class="alert alert-warning" id="category-no-topics">[[recent:no_popular_topics]]</div>
			<!-- ELSE -->
   			<!-- IMPORT partials/topic_list2.tpl -->
			<!-- ENDIF !popularData.topics.length -->

			<!-- IF config.usePagination -->
				<!-- IMPORT partials/paginator.tpl -->
			<!-- ENDIF config.usePagination -->
		</div>
	</div>

	<div data-widget-area="sidebar" class="col-lg-3 col-sm-12 <!-- IF !widgets.sidebar.length -->hidden<!-- ENDIF !widgets.sidebar.length -->">
		{{{each widgets.sidebar}}}
		{{widgets.sidebar.html}}
		{{{end}}}
	</div>
</div>

<div data-widget-area="footer">
	{{{each widgets.footer}}}
	{{widgets.footer.html}}
	{{{end}}}
</div>

