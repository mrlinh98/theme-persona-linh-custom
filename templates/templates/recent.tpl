<!-- IMPORT partials/breadcrumbs.tpl -->
<div data-widget-area="header">
	{{{each widgets.header}}}
	{{widgets.header.html}}
	{{{end}}}
</div>

<div class="btn-toolbar" <!-- IF isHome -->style="display:none;"<!-- ENDIF isHome -->>
	<div class="pull-left">
		<!-- IF canPost -->
		<!-- IMPORT partials/buttons/newTopic.tpl -->
		<!-- ELSE -->
		<a component="category/post/guest" href="{config.relative_path}/login" class="btn btn-primary">[[category:guest-login-post]]</a>
		<!-- ENDIF canPost -->
		<a href="{config.relative_path}/{selectedFilter.url}" class="inline-block">
			<div class="alert alert-warning hide" id="new-topics-alert"></div>
		</a>
	</div>

	<!-- IMPORT partials/category-filter.tpl -->

	<div class="btn-group pull-right bottom-sheet <!-- IF !filters.length -->hidden<!-- ENDIF !filters.length -->">
		<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
		{selectedFilter.name} <span class="caret"></span>
		</button>
		<ul class="dropdown-menu" role="menu">
			{{{each filters}}}
			<li role="presentation" class="category">
				<a role="menu-item" href="{config.relative_path}/{filters.url}"><i class="fa fa-fw <!-- IF filters.selected -->fa-check<!-- ENDIF filters.selected -->"></i>{filters.name}</a>
			</li>
			{{{end}}}
		</ul>
	</div>
</div>

<div class="row">
	<div class="recent col-sm-12 col-lg-12">
		<h1 class="categories-title" <!-- IF isRecent -->style="display:none;"<!-- ENDIF isRecent -->>[[pages:recents]]</h1>
		<div class="category">
			<!-- IF !topics.length -->
			<div class="alert alert-warning" id="category-no-topics">[[recent:no_recent_topics]]</div>
			<!-- ENDIF !topics.length -->

			<!-- IMPORT partials/topics_list.tpl -->

			<!-- IF config.usePagination -->
				<!-- IMPORT partials/paginator.tpl -->
			<!-- ENDIF config.usePagination -->
		</div>
	</div>
</div>

{{{each groups}}}
	<div class="col-lg-4 col-md-6 col-sm-12" component="groups/summary" data-slug="{groups.slug}">
		<div class="panel panel-default">
			<a href="{config.relative_path}/groups/{groups.slug}" class="panel-heading list-cover" style="<!-- IF groups.cover:thumb:url -->background-image: url({groups.cover:thumb:url});<!-- ENDIF groups.cover:thumb:url -->">
				<h3 class="panel-title">{groups.displayName} <small>{groups.memberCount}</small></h3>
			</a>
			<div class="panel-body">
				<ul class="members">
					{{{each groups.members}}}
					<li>
						<a href="{config.relative_path}/user/{groups.members.userslug}">{buildAvatar(groups.members, "sm", true)}</a>
					</li>
					{{{end}}}
					<!-- IF groups.truncated -->
					<li class="truncated"><i class="fa fa-ellipsis-h"></i></li>
					<!-- ENDIF groups.truncated -->
				</ul>
			</div>
		</div>
	</div>
{{{end}}}