<!-- IF ../isSection -->
{../name}
<!-- ELSE -->
  <!-- IF ../link -->
  <a href="{../link}" itemprop="url" data='a{../slug}'>
  <!-- ELSE -->
  <a href="{config.relative_path}/category/{../slug}" data='{../name}' itemprop="url">
  <!-- ENDIF ../link -->
  {../name}
  </a>
<!-- ENDIF ../isSection -->