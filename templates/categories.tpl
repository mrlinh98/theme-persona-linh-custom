<!-- IMPORT partials/breadcrumbs.tpl -->
<div data-widget-area="header">
	{{{each widgets.header}}}
	{{widgets.header.html}}
	{{{end}}}
</div>

<div class="row">
	<div class="<!-- IF isCategories -->col-lg-12<!-- ELSE -->col-lg-4<!-- ENDIF isCategories -->">
		<div>
			<div class="black-line"></div>
			<ul class="categories" itemscope itemtype="http://www.schema.org/ItemList" style="box-shadow: none; border: 1px solid rgba(195, 200, 218, 0.38); padding-bottom: 25px;">
				<h1 class="categories-title" style="margin-top: 20px; padding-left: 4%;">[[pages:categories]]</h1>

				<div class="small-line"></div>
				{{{each categories}}}
						<!-- IMPORT partials/categories/item.tpl -->
				{{{end}}}
			</ul>
			<a class="see-more perfect-center <!-- IF isCategories --> display-none <!-- ENDIF isCategories -->" href="/categories">Xem Tất Cả</a>
		</div>

		<!-- IF isHome -->
			<div style="margin-top: 2rem">
				<div class="black-line"></div>
				<!-- IF groupData.groups.length -->
					<!-- IMPORT partials/groups/list-custom.tpl -->
				<!-- ELSE -->
				<div class="col-xs-12">
					<div class="alert alert-warning">
					[[groups:no_groups_found]]
					</div>
				</div>
				<!-- ENDIF groups.length -->
			</div>
		<!-- ENDIF isHome -->
	</div>

	<div class="recent col-sm-12 col-lg-8" <!-- IF isCategories -->style="display:none;"<!-- ENDIF isCategories -->>
		<div class="category">
			<div class="black-line"></div>

			<!-- IF !recentData.topics.length -->
				<div class="alert alert-warning" id="category-no-topics">[[recent:no_recent_topics]]</div>
			<!-- ELSE -->
				<!-- IMPORT partials/topic_list1.tpl -->
			<!-- ENDIF !recentData.topics.length -->

			<!-- IF config.usePagination -->
				<!-- IMPORT partials/paginator.tpl -->
			<!-- ENDIF config.usePagination -->
		</div>
		<a class="see-more perfect-center" href="/recent">Xem Tất Cả</a>
	</div>

	<br />
	<br />

	<div class="popular col-sm-12 col-lg-12"  <!-- IF isCategories -->style="display:none;"<!-- ENDIF isCategories -->>
		<div class="category">
			<div class="black-line"></div>
			<!-- IF !popularData.topics.length -->
				<div class="alert alert-warning" id="category-no-topics">[[recent:no_popular_topics]]</div>
			<!-- ELSE -->
   			<!-- IMPORT partials/topic_list2.tpl -->
			<!-- ENDIF !popularData.topics.length -->

			<!-- IF config.usePagination -->
				<!-- IMPORT partials/paginator.tpl -->
			<!-- ENDIF config.usePagination -->
		</div>
		<a class="see-more perfect-center" href="/popular">Xem Tất Cả</a>
	</div>

	

	<div data-widget-area="sidebar" class="col-lg-3 col-sm-12 <!-- IF !widgets.sidebar.length -->hidden<!-- ENDIF !widgets.sidebar.length -->">
		{{{each widgets.sidebar}}}
		{{widgets.sidebar.html}}
		{{{end}}}
	</div>
</div>

<div data-widget-area="footer">
	{{{each widgets.footer}}}
	{{widgets.footer.html}}
	{{{end}}}
</div>

